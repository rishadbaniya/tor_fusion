
# tor_fusion: parsing tor network data with [arrow](https://docs.rs/arrow/latest/arrow/) and [datafusion](https://docs.rs/datafusion/28.0.0/datafusion/index.html).

tor_fusion is a project to parse [Tor](https://www.torproject.org/) network
documents in the [Rust](https://www.rust-lang.org/) programming language.

## Links:

This is the README for the tor_fusion project as a whole.
If you want find more practical information regarding parsing Tor network documents
and extracting metrics you might want to check out these links:

  * [Tor Metrics website](https://metrics.torproject.org/)

  * [Docs on reproducible metrics](https://metrics.torproject.org/reproducible-metrics.html)

  * [DescriptorParser](https://gitlab.torproject.org/tpo/network-health/metrics/descriptorParser/)
  a java app to parse and store Tor network documents.

  * [Metrics Library](https://gitlab.torproject.org/tpo/network-health/metrics/library)
  a java library to parse Tor network documents

  * [Collector](https://collector.torproject.org) an archive of data from
  various nodes and services in the public Tor network.


## Why rewrite how network documents are parsed?

The data analysis community is evolving and moving to different tools then
when the metrics pipeline was first developed.

At the same time we have way more data produced from nodes and services on the
public Tor network then when metrics started as a project in Tor.

We are in the process of [restructuring our pipeline](https://gitlab.torproject.org/tpo/network-health/team/-/wikis/metrics/collector/pipeline)
so that is easier to maintain over time, but also so that we are able to offer
better resources to our community and process data more efficiently.

Rust stands out as a practical choice for processing Tor network metrics, due to
 its performance and security features.

Rust offers efficiency when handling large datasets. Additionally, developers
can use an array of libraries explicitly designed for data analysis to
streamline data processing that is not specific to Tor, offering more
flexibility to researchers.


## What documents are supported?

We are currently only parsing onionperf analysis files. The long term plan is to
embed [Arti](https://gitlab.torproject.org/tpo/core/arti/) to download and parse
all types of documents produced by the various network nodes and services.
