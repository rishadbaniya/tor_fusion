mod parse;
mod tgen_stream;
mod tor_circuit;

use anyhow::{Result};

use arrow::util::display::{ArrayFormatter, FormatOptions};
use arrow_array::RecordBatch;
use arrow_json;
use data_encoding::HEXUPPER;
use datafusion::prelude::SessionContext;
use parse::Parse;
use ring::digest::{Context, Digest, SHA256};
use std::{
    fs::File,
    fs,
    io::BufReader,
    io::Read,
    sync::Arc,
    time::{SystemTime, UNIX_EPOCH, Duration},
};

use tgen_stream::TgenStream;

use tokio_postgres::{NoTls, Client};

use tor_circuit::TorCircuit;

fn sha256_digest<R: Read>(mut reader: R) -> Result<Digest> {
    let mut context = Context::new(&SHA256);
    let mut buffer = [0; 1024];

    loop {
        let count = reader.read(&mut buffer)?;
        if count == 0 {
            break;
        }
        context.update(&buffer[..count]);
    }

    Ok(context.finish())
}


fn parse_to_arrow_batch(path: &str, schema: Arc<arrow_schema::Schema>) -> RecordBatch {
    let file = File::open(path).unwrap();

    let mut json = arrow_json::ReaderBuilder::new(schema).with_coerce_primitive(true).build(BufReader::new(file)).unwrap();
    let batch = json.next().unwrap().unwrap();

    batch
}
async fn add_stream(client: &Client, node_id: &String, digest: &String, st: &TgenStream) -> Result<u64, tokio_postgres::Error> {
    let unix_ts_end = UNIX_EPOCH + Duration::from_secs(st.unix_ts_end as u64);
    let unix_ts_start = UNIX_EPOCH + Duration::from_secs(st.unix_ts_start as u64);
    let code = client.execute(
        r#"INSERT INTO onionperf_tgen_stream
        (onionperf_node, stream_id, byte_info_payload_bytes_recv,
         byte_info_payload_bytes_send, byte_info_payload_progress_recv,
         byte_info_payload_progress_send, byte_info_total_bytes_recv,
         byte_info_total_bytes_send, elapsed_seconds_payload_bytes_recv,
         elapsed_seconds_payload_bytes_send, elapsed_seconds_payload_progress_recv,
         elapsed_seconds_payload_progress_sent, is_complete, is_error, is_success,
         stream_info_error, stream_info_id, stream_info_name, stream_info_peername,
         stream_info_recvsize, stream_info_recvstate, stream_info_sendsize,
         stream_info_sendstate, stream_info_vertexid, time_info_created_ts,
         time_info_now_ts, time_info_usecs_to_checksum_recv,
         time_info_usecs_to_checksum_send, time_info_usecs_to_command,
         time_info_usecs_to_first_byte_recv, time_info_usecs_to_first_byte_send,
         time_info_usecs_to_last_byte_recv, time_info_usecs_to_last_byte_send,
         time_info_usecs_to_proxy_choice, time_info_usecs_to_proxy_init,
         time_info_usecs_to_proxy_request, time_info_usecs_to_proxy_response,
         time_info_usecs_to_response, time_info_usecs_to_socket_connect,
         time_info_usecs_to_socket_create, transport_info_error, transport_info_fd,
         transport_info_local, transport_info_proxy, transport_info_remote,
         transport_info_state, unix_ts_end, unix_ts_start, onionperf_analysis) VALUES
         ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10,
         $11, $12, $13, $14, $15, $16, $17, $18, $19, $20,
         $21, $22, $23, $24, $25, $26, $27, $28, $29, $30,
         $31, $32, $33, $34, $35, $36, $37, $38, $39, $40,
         $41, $42, $43, $44, $45, $46, $47, $48, $49)"#,
        &[node_id, &st.stream_id, &st.byte_info_payload_bytes_recv, &st.byte_info_payload_bytes_send,
        &st.byte_info_payload_progress_recv, &st.byte_info_payload_progress_send,
        &st.byte_info_total_bytes_recv, &st.byte_info_total_bytes_send,
        &st.elapsed_seconds_payload_bytes_recv, &st.elapsed_seconds_payload_bytes_send,
        &st.elapsed_seconds_payload_progress_recv, &st.elapsed_seconds_payload_progress_send,
        &st.is_complete, &st.is_error, &st.is_success, &st.stream_info_error, &st.stream_info_id,
        &st.stream_info_name, &st.stream_info_peername, &st.stream_info_recvsize,
        &st.stream_info_recvstate, &st.stream_info_sendsize, &st.stream_info_sendstate, &st.stream_info_vertexid,
        &st.time_info_created_ts, &st.time_info_now_ts, &st.time_info_usecs_to_checksum_recv,
        &st.time_info_usecs_to_checksum_send, &st.time_info_usecs_to_command,
        &st.time_info_usecs_to_first_byte_recv, &st.time_info_usecs_to_first_byte_send,
        &st.time_info_usecs_to_last_byte_recv, &st.time_info_usecs_to_last_byte_send,
        &st.time_info_usecs_to_proxy_choice, &st.time_info_usecs_to_proxy_init,
        &st.time_info_usecs_to_proxy_request, &st.time_info_usecs_to_proxy_response,
        &st.time_info_usecs_to_response, &st.time_info_usecs_to_socket_connect,
        &st.time_info_usecs_to_socket_create, &st.transport_info_error, &st.transport_info_fd,
        &st.transport_info_local, &st.transport_info_proxy, &st.transport_info_remote,
        &st.transport_info_state, &unix_ts_end, &unix_ts_start, &digest],
    ).await;

    code
}
async fn insert_streams(client: &Client, record: RecordBatch, node_id: &String, digest: &String, streams: Vec<String>) -> Result<()> {
    for stream in streams.iter() {
        let st = TgenStream::create(record.clone(), node_id, &stream).await?;
        let code = add_stream(client, node_id, digest, &st).await;
        println!("{}", code.unwrap());
    }
    Ok(())
}

async fn insert_circuits(client: &Client, record: RecordBatch, node_id: &String, digest: &String, circuits: Vec<String>) -> Result<()> {
    for circuit in circuits.iter() {
        let tc = TorCircuit::create(record.clone(), node_id, circuit).await?;
        let unix_ts_end = UNIX_EPOCH + Duration::from_secs(tc.unix_ts_end as u64);
        let unix_ts_start = UNIX_EPOCH + Duration::from_secs(tc.unix_ts_start as u64);
        let code = client.execute(
            r#"INSERT INTO onionperf_tor_circuit
            (onionperf_node, circuit_id, build_quantile, build_timeout, buildtime_seconds,
            cbt_set, current_guards, elapsed_seconds, failure_reason_local, filtered_out,
            circuit_path, unix_ts_end, unix_ts_start, onionperf_analysis) VALUES
            ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)"#,
            &[node_id, &circuit, &tc.build_quantile, &tc.build_timeout, &tc.buildtime_seconds,
            &tc.cbt_set, &tc.current_guards, &tc.elapsed_seconds, &tc.failure_reason_local,
            &tc.filtered_out, &tc.path, &unix_ts_end, &unix_ts_start, &digest],
        ).await;
        println!("{}", code.unwrap());
    }
    Ok(())
}

async fn get_analysis_key (record: RecordBatch, col: &str) -> Result<String> {
    let results: Vec<RecordBatch> = {
        let ctx = SessionContext::new();
        let _ = ctx.register_batch("t", record);

        let query = format!("SELECT {} FROM t", col);

        let df = ctx.sql(&query).await?;
        df.collect().await?
    };

    let lookup_data = &results[0];
    let mut columns = vec![];
    for column in lookup_data.columns().iter() {
        columns.push(column);
    }
    let options = FormatOptions::default();
    let formatter = ArrayFormatter::try_new(columns[0].as_ref(), &options)?;
    Ok(formatter.value(0).to_string())
}

fn get_analysis_published(path: &str) -> SystemTime {
    let sys_time = fs::metadata(path).unwrap().modified();
    let tm = sys_time.unwrap();
    tm
}

async fn insert_analysis(client: &Client, path: &str, record: RecordBatch,
        node_id: &String, digest: &String) -> Result<u64, tokio_postgres::Error> {
    let col = format!("data['{}']['{}']", node_id, "measurement_ip");
    let measurement_ip = get_analysis_key(record.clone(), &col).await.unwrap();
    let op_type = get_analysis_key(record.clone(), "type").await.unwrap();
    let filters = get_analysis_key(record.clone(), "filters").await.unwrap();
    let version = get_analysis_key(record.clone(), "version").await.unwrap();
    let published = get_analysis_published(path);
    let code = client.execute(
        r#"INSERT INTO onionperf_analysis
        (onionperf_node, published, measurement_ip, type, version, filters, digest) VALUES
        ($1, $2, $3, $4, $5, $6, $7)"#,
        &[node_id, &published, &measurement_ip, &op_type, &version, &filters, &digest],
    ).await;

    code
}

async fn get_analysis_digest(path: &str) -> Result<String> {
    let input = File::open(path)?;
    let reader = BufReader::new(input);
    let digest = sha256_digest(reader)?;

    Ok(HEXUPPER.encode(digest.as_ref()))
}

#[tokio::main]
pub async fn run(path: &str) -> Result<()>  {
    let parse = Parse::new(path);
    let schema = parse.build_schema();
    let batch = parse_to_arrow_batch(&path, schema);
    let node_id = parse.node_id;
    let streams = parse.streams;
    let circuits = parse.circuits;
    let digest = get_analysis_digest(path).await.unwrap();

    // Connect to the database.
    let (client, connection) =
        tokio_postgres::connect("host=postgresdb dbname=metrics user=metrics password='password' connect_timeout=10 keepalives=0", NoTls).await?;

    // The connection object performs the actual communication with the database,
    // so spawn it off to run on its own.
    tokio::spawn(async move {
        if let Err(e) = connection.await {
            eprintln!("connection error: {}", e);
        }
    });

    let _ = insert_analysis(&client, &path, batch.clone(), &node_id, &digest).await;
    let _ = insert_streams(&client, batch.clone(), &node_id, &digest, streams).await;
    let _ = insert_circuits(&client, batch.clone(), &node_id, &digest, circuits).await;

    Ok(())
}
