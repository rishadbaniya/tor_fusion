use clap::Parser;

#[derive(Parser, Debug)]
pub struct Cli {
    pub path: String,
}
