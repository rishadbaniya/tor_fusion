use anyhow::Result;

use arrow_array::RecordBatch;
use arrow_json::writer::array_to_json_array;
use arrow_schema::ArrowError;

use datafusion::prelude::SessionContext;

use serde_json;
use serde_json::Value;

#[derive(Debug, Default)]
pub struct TgenStream {
    pub stream_id: String,
    pub byte_info_payload_bytes_recv: i64,
    pub byte_info_payload_bytes_send: i64,
    pub byte_info_payload_progress_recv: String,
    pub byte_info_payload_progress_send: String,
    pub byte_info_total_bytes_recv: i64,
    pub byte_info_total_bytes_send: i64,
    pub elapsed_seconds_payload_bytes_recv: String,
    pub elapsed_seconds_payload_bytes_send: String,
    pub elapsed_seconds_payload_progress_recv: String,
    pub elapsed_seconds_payload_progress_send: String,
    pub is_complete: bool,
    pub is_error: bool,
    pub is_success: bool,
    pub stream_info_error: String,
    pub stream_info_id: i64,
    pub stream_info_name: String,
    pub stream_info_peername: String,
    pub stream_info_recvsize: i64,
    pub stream_info_sendsize: i64,
    pub stream_info_recvstate: String,
    pub stream_info_sendstate: String,
    pub stream_info_vertexid: String,
    pub time_info_created_ts: i64,
    pub time_info_now_ts: i64,
    pub time_info_usecs_to_checksum_recv: i64,
    pub time_info_usecs_to_checksum_send: i64,
    pub time_info_usecs_to_command: i64,
    pub time_info_usecs_to_first_byte_recv: i64,
    pub time_info_usecs_to_first_byte_send: i64,
    pub time_info_usecs_to_last_byte_recv: i64,
    pub time_info_usecs_to_last_byte_send: i64,
    pub time_info_usecs_to_proxy_choice: i64,
    pub time_info_usecs_to_proxy_init: i64,
    pub time_info_usecs_to_proxy_request: i64,
    pub time_info_usecs_to_proxy_response: i64,
    pub time_info_usecs_to_response: i64,
    pub time_info_usecs_to_socket_connect: i64,
    pub time_info_usecs_to_socket_create: i64,
    pub transport_info_error: String,
    pub transport_info_fd: i64,
    pub transport_info_local: String,
    pub transport_info_proxy: String,
    pub transport_info_remote: String,
    pub transport_info_state: String,
    pub unix_ts_start: f64,
    pub unix_ts_end: f64,
}

impl TgenStream {
    async fn extract_data<'a>(
        record: RecordBatch, node_id: &'a str, data: &'a str,
        container: &'a str, id: &'a str, key: &'a str) -> Result<Vec<Value>, ArrowError> {
        let results: Vec<RecordBatch> = {
            let ctx = SessionContext::new();
            let _ = ctx.register_batch("t", record);
            let col = format!("data['{}']['{}']['{}']['{}']['{}']", node_id, data, container, id, key);
            let query = format!("SELECT {} FROM t", col);

            let df = ctx.sql(&query).await?;
            df.collect().await?
        };

        let lookup_data = &results[0];
        let mut columns = vec![];
        for column in lookup_data.columns().iter() {
            columns.push(column);
        }

        let json_data = array_to_json_array(columns[0]);
        json_data

    }

    pub async fn create(record: RecordBatch, node_id: &String, stream: &String) -> Result<TgenStream> {
        let byte_info = &Self::extract_data(record.clone(), &node_id.to_string(), "tgen",
            "streams", &stream.to_string(), "byte_info").await?[0];

        let elapsed_seconds = &Self::extract_data(record.clone(), &node_id.to_string(), "tgen",
            "streams", &stream.to_string(), "elapsed_seconds").await?[0];

        let is_complete: bool = Self::extract_data(record.clone(), &node_id.to_string(), "tgen",
            "streams", &stream.to_string(), "is_complete").await?[0].to_string().parse().unwrap();

        let is_error: bool = Self::extract_data(record.clone(), &node_id.to_string(), "tgen",
            "streams", &stream.to_string(), "is_error").await?[0].to_string().parse().unwrap();

        let is_success: bool = Self::extract_data(record.clone(), &node_id.to_string(), "tgen",
            "streams", &stream.to_string(), "is_success").await?[0].to_string().parse().unwrap();

        let stream_info = &Self::extract_data(record.clone(), &node_id.to_string(), "tgen",
            "streams", &stream.to_string(), "stream_info").await?[0];

        let time_info = &Self::extract_data(record.clone(), &node_id.to_string(), "tgen",
            "streams", &stream.to_string(), "time_info",).await?[0];

        let transport_info = &Self::extract_data(record.clone(), &node_id.to_string(), "tgen",
            "streams", &stream.to_string(), "transport_info").await?[0];

        let unix_ts_start: f64 = Self::extract_data(record.clone(), &node_id.to_string(), "tgen",
            "streams", &stream.to_string(), "unix_ts_start").await?[0].to_string().parse().unwrap();

        let unix_ts_end: f64 = Self::extract_data(record.clone(), &node_id.to_string(), "tgen",
            "streams", &stream.to_string(), "unix_ts_end").await?[0].to_string().parse().unwrap();

        let ts = TgenStream {
            stream_id: stream.to_string(),
            byte_info_payload_bytes_recv: byte_info["payload-bytes-recv"].as_i64().unwrap(),
            byte_info_payload_bytes_send: byte_info["payload-bytes-send"].as_i64().unwrap(),
            byte_info_payload_progress_recv: byte_info["payload-progress-recv"].to_string(),
            byte_info_payload_progress_send: byte_info["payload-progress-send"].to_string(),
            byte_info_total_bytes_recv: byte_info["total-bytes-recv"].as_i64().unwrap(),
            byte_info_total_bytes_send: byte_info["total-bytes-send"].as_i64().unwrap(),
            elapsed_seconds_payload_bytes_recv: elapsed_seconds["payload_bytes_recv"].to_string(),
            elapsed_seconds_payload_bytes_send: elapsed_seconds["payload_bytes_send"].to_string(),
            elapsed_seconds_payload_progress_recv: elapsed_seconds["payload_progress_recv"].to_string(),
            elapsed_seconds_payload_progress_send: elapsed_seconds["payload_progress_send"].to_string(),
            is_complete: is_complete,
            is_error: is_error,
            is_success: is_success,
            stream_info_error: stream_info["error"].to_string(),
            stream_info_id: if stream_info["id"].is_null() { -1 } else { stream_info["id"].as_i64().unwrap()},
            stream_info_name: stream_info["name"].to_string(),
            stream_info_peername: stream_info["peername"].to_string(),
            stream_info_recvsize: if stream_info["recvsize"].is_null() { -1 } else { stream_info["recvsize"].as_i64().unwrap() },
            stream_info_sendsize: if stream_info["sendsize"].is_null() { -1 } else { stream_info["sendsize"].as_i64().unwrap() },
            stream_info_recvstate: stream_info["recvstate"].to_string(),
            stream_info_sendstate: stream_info["sendstate"].to_string(),
            stream_info_vertexid: stream_info["vertexid"].to_string(),
            time_info_created_ts: if time_info["created_ts"].is_null() { -1 } else { time_info["created_ts"].as_i64().unwrap() },
            time_info_now_ts: if time_info["now_ts"].is_null() { -1 } else { time_info["now_ts"].as_i64().unwrap() },
            time_info_usecs_to_checksum_recv: if time_info["usecs_to_checksum_recv"].is_null() { -1 } else { time_info["usecs_to_checksum_recv"].as_i64().unwrap() },
            time_info_usecs_to_checksum_send: if time_info["usecs_to_checksum_send"].is_null() { -1 } else { time_info["usecs_to_checksum_send"].as_i64().unwrap() },
            time_info_usecs_to_command: if time_info["usecs_to_command"].is_null() { -1 } else { time_info["usecs_to_command"].as_i64().unwrap() },
            time_info_usecs_to_first_byte_recv: if time_info["time_info_usecs_to_first_byte_recv"].is_null() { -1 } else { time_info["time_info_usecs_to_first_byte_recv"].as_i64().unwrap() },
            time_info_usecs_to_first_byte_send: if time_info["time_info_usecs_to_first_byte_send"].is_null() { -1 } else { time_info["time_info_usecs_to_first_byte_send"].as_i64().unwrap() },
            time_info_usecs_to_last_byte_recv: if time_info["time_info_usecs_to_last_byte_recv"].is_null() { -1 } else { time_info["time_info_usecs_to_last_byte_recv"].as_i64().unwrap() },
            time_info_usecs_to_last_byte_send: if time_info["time_info_usecs_to_last_byte_send"].is_null() { -1 } else { time_info["time_info_usecs_to_last_byte_send"].as_i64().unwrap() },
            time_info_usecs_to_proxy_choice: if time_info["time_info_usecs_to_proxy_choice"].is_null() { -1 } else { time_info["time_info_usecs_to_proxy_choice"].as_i64().unwrap() },
            time_info_usecs_to_proxy_init: if time_info["time_info_usecs_to_proxy_init"].is_null() { -1 } else { time_info["time_info_usecs_to_proxy_init"].as_i64().unwrap() },
            time_info_usecs_to_proxy_request: if time_info["time_info_usecs_to_proxy_request"].is_null() { -1 } else { time_info["time_info_usecs_to_proxy_request"].as_i64().unwrap() },
            time_info_usecs_to_proxy_response: if time_info["time_info_usecs_to_proxy_response"].is_null() { -1 } else { time_info["time_info_usecs_to_proxy_response"].as_i64().unwrap() },
            time_info_usecs_to_response: if time_info["time_info_usecs_to_response"].is_null() { -1 } else { time_info["time_info_usecs_to_response"].as_i64().unwrap() },
            time_info_usecs_to_socket_connect: if time_info["time_info_usecs_to_socket_connect"].is_null() { -1 } else { time_info["time_info_usecs_to_socket_connect"].as_i64().unwrap() },
            time_info_usecs_to_socket_create: if time_info["time_info_usecs_to_socket_create"].is_null() { -1 } else { time_info["time_info_usecs_to_socket_create"].as_i64().unwrap() },
            transport_info_error: transport_info["error"].to_string(),
            transport_info_fd: if transport_info["fd"].is_null() { -1 } else { transport_info["fd"].as_i64().unwrap() },
            transport_info_local: transport_info["local"].to_string(),
            transport_info_proxy: transport_info["proxy"].to_string(),
            transport_info_remote: transport_info["remote"].to_string(),
            transport_info_state: transport_info["state"].to_string(),
            unix_ts_start: unix_ts_start,
            unix_ts_end: unix_ts_end,
        };
        Ok(ts)
    }
}
