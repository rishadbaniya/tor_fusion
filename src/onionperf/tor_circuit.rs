use anyhow::Result;

use arrow_array::RecordBatch;
use arrow::util::display::{ArrayFormatter, FormatOptions};

use datafusion::prelude::SessionContext;

#[derive(Debug, Default)]
pub struct TorCircuit {
    pub node_id: String,
    pub build_quantile: f64,
    pub build_timeout: i64,
    pub buildtime_seconds: f64,
    pub cbt_set: bool,
    pub circuit_id: i64,
    pub current_guards: String,
    pub elapsed_seconds: String,
    pub failure_reason_local: String,
    pub filtered_out: bool,
    pub path: String,
    pub unix_ts_end: f64,
    pub unix_ts_start: f64,
}

impl TorCircuit {
    async fn extract_data<'a>(
        record: RecordBatch, node_id: &'a str, data: &'a str,
        container: &'a str, id: &'a str, key: &'a str) -> Result<String> {
        let results: Vec<RecordBatch> = {
            let ctx = SessionContext::new();
            let _ = ctx.register_batch("t", record);
            let col = format!("data['{}']['{}']['{}']['{}']['{}']", node_id, data, container, id, key);
            let query = format!("SELECT {} FROM t", col);

            let df = ctx.sql(&query).await?;
            df.collect().await?
        };

        let lookup_data = &results[0];
        let mut columns = vec![];
        for column in lookup_data.columns().iter() {
            columns.push(column);
        }
        let options = FormatOptions::default();
        let formatter = ArrayFormatter::try_new(columns[0].as_ref(), &options)?;
        Ok(formatter.value(0).to_string())

    }

    pub async fn create(record: RecordBatch, node_id: &String, circuit: &String) -> Result<TorCircuit> {
        let bq = Self::extract_data(record.clone(), &node_id.to_string(), "tor",
            "circuits", &circuit.to_string(), "build_quantile").await?;
        let build_quantile: f64 = if bq.is_empty() {
            0.0
        } else {
            bq.parse().unwrap()
        };
        let bt = Self::extract_data(record.clone(), &node_id.to_string(), "tor",
            "circuits", &circuit.to_string(), "build_timeout").await?;
        let build_timeout: i64 = if bt.is_empty() {
            -1
        } else {
            bt.parse().unwrap()
        };
        let bs = Self::extract_data(record.clone(), &node_id.to_string(), "tor",
            "circuits", &circuit.to_string(), "buildtime_seconds").await?;
        let buildtime_seconds: f64 = if bs.is_empty() {
            0.0
        } else {
            bs.parse().unwrap()
        };
        let cbt_set: bool = Self::extract_data(record.clone(), &node_id.to_string(), "tor",
            "circuits", &circuit.to_string(), "cbt_set").await?.parse().unwrap();
        let circuit_id: i64 = Self::extract_data(record.clone(), &node_id.to_string(), "tor",
            "circuits", &circuit.to_string(), "circuit_id").await?.parse().unwrap();
        let current_guards = Self::extract_data(record.clone(), &node_id.to_string(), "tor",
            "circuits", &circuit.to_string(), "current_guards").await?;
        let elapsed_seconds = Self::extract_data(record.clone(), &node_id.to_string(), "tor",
            "circuits", &circuit.to_string(), "elapsed_seconds").await?;
        let failure_reason_local = Self::extract_data(record.clone(), &node_id.to_string(), "tor",
            "circuits", &circuit.to_string(), "failure_reason_local").await?;
        let filtered_out: bool = Self::extract_data(record.clone(), &node_id.to_string(), "tor",
            "circuits", &circuit.to_string(), "filtered_out").await?.parse().unwrap();
        let path = Self::extract_data(record.clone(), &node_id.to_string(), "tor",
            "circuits", &circuit.to_string(), "path").await?;
        let unix_ts_end: f64 = Self::extract_data(record.clone(), &node_id.to_string(), "tor",
            "circuits", &circuit.to_string(), "unix_ts_end").await?.parse().unwrap();
        let unix_ts_start: f64 = Self::extract_data(record.clone(), &node_id.to_string(), "tor",
            "circuits", &circuit.to_string(), "unix_ts_start").await?.parse().unwrap();

        let tc = TorCircuit {
            node_id: node_id.to_string(),
            build_quantile: build_quantile,
            build_timeout: build_timeout,
            buildtime_seconds: buildtime_seconds,
            cbt_set: cbt_set,
            circuit_id: circuit_id,
            current_guards: current_guards,
            elapsed_seconds: elapsed_seconds,
            failure_reason_local: failure_reason_local,
            filtered_out: filtered_out,
            path: path,
            unix_ts_end: unix_ts_end,
            unix_ts_start: unix_ts_start,
        };
        Ok(tc)

    }
}
