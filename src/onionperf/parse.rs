use arrow_schema::{DataType, Field, Schema};
use serde_json;
use serde_json::Value;

use std::{collections::HashMap, fs, sync::Arc, vec};

#[derive(Debug, Default)]
pub struct Parse {
    pub node_id: String,
    pub path: String,
    pub streams: Vec<String>,
    pub circuits: Vec<String>,
    pub json: serde_json::Value,
}

impl Parse {
    /// Generate dictionary structs for stream json objects
    fn generate_stream_dictionary(stream_id: &str) -> Field {
        let byte_info = Field::new_struct(
            "byte_info",
            vec![
                Field::new("payload-bytes-recv", DataType::Int64, false),
                Field::new("payload-bytes-send", DataType::Int64, false),
                Field::new("payload-progress-recv", DataType::Utf8, false),
                Field::new("payload-progress-send", DataType::Utf8, false),
                Field::new("total-bytes-recv", DataType::Int64, false),
                Field::new("total-bytes-send", DataType::Int64, false),
            ],
            false,
        );

        let payload_bytes_recv = Field::new_struct(
            "payload_bytes_recv",
            vec![
                Field::new("10240", DataType::Float64, false),
                Field::new("20480", DataType::Float64, false),
                Field::new("51200", DataType::Float64, false),
                Field::new("102400", DataType::Float64, false),
                Field::new("204800", DataType::Float64, false),
                Field::new("512000", DataType::Float64, false),
                Field::new("1048576", DataType::Float64, false),
                Field::new("2097152", DataType::Float64, false),
                Field::new("5242880", DataType::Float64, false),
            ],
            false,
        );

        let payload_bytes_send = Field::new_struct(
            "payload_bytes_send",
            vec![
                Field::new("10240", DataType::Float64, true),
                Field::new("20480", DataType::Float64, true),
                Field::new("51200", DataType::Float64, true),
                Field::new("102400", DataType::Float64, true),
                Field::new("204800", DataType::Float64, true),
                Field::new("512000", DataType::Float64, true),
                Field::new("1048576", DataType::Float64, true),
                Field::new("2097152", DataType::Float64, true),
                Field::new("5242880", DataType::Float64, true),
            ],
            true,
        );

        let payload_progress_recv = Field::new_struct(
            "payload_progress_recv",
            vec![
                Field::new("0.0", DataType::Float64, false),
                Field::new("0.1", DataType::Float64, false),
                Field::new("0.2", DataType::Float64, false),
                Field::new("0.3", DataType::Float64, false),
                Field::new("0.4", DataType::Float64, false),
                Field::new("0.5", DataType::Float64, false),
                Field::new("0.6", DataType::Float64, false),
                Field::new("0.7", DataType::Float64, false),
                Field::new("0.8", DataType::Float64, false),
                Field::new("0.9", DataType::Float64, false),
                Field::new("1.0", DataType::Float64, false),
            ],
            false,
        );

        let payload_progress_send = Field::new_struct(
            "payload_progress_send",
            vec![
                Field::new("0.0", DataType::Float64, false),
                Field::new("0.1", DataType::Float64, false),
                Field::new("0.2", DataType::Float64, false),
                Field::new("0.3", DataType::Float64, false),
                Field::new("0.4", DataType::Float64, false),
                Field::new("0.5", DataType::Float64, false),
                Field::new("0.6", DataType::Float64, false),
                Field::new("0.7", DataType::Float64, false),
                Field::new("0.8", DataType::Float64, false),
                Field::new("0.9", DataType::Float64, false),
                Field::new("1.0", DataType::Float64, false),
            ],
            false,
        );

        let elapsed_seconds = Field::new_struct(
            "elapsed_seconds",
            vec![
                payload_bytes_recv,
                payload_bytes_send,
                payload_progress_recv,
                payload_progress_send,
            ],
            true,
        );

        let stream_info = Field::new_struct(
            "stream_info",
            vec![
                Field::new("error", DataType::Utf8, false),
                Field::new("id", DataType::Int64, false),
                Field::new("name", DataType::Utf8, false),
                Field::new("peername", DataType::Utf8, false),
                Field::new("recvsize", DataType::Int64, false),
                Field::new("sendsize", DataType::Int64, false),
                Field::new("recvstate", DataType::Utf8, false),
                Field::new("sendstate", DataType::Utf8, false),
                Field::new("vertexid", DataType::Utf8, false),
            ],
            false,
        );

        let time_info = Field::new_struct(
            "time_info",
            vec![
                Field::new("created-ts", DataType::Int64, false),
                Field::new("now-ts", DataType::Int64, false),
                Field::new("usecs-to-checksum-recv", DataType::Int64, false),
                Field::new("usecs-to-checksum-send", DataType::Int64, false),
                Field::new("usecs-to-command", DataType::Int64, false),
                Field::new("usecs-to-first-byte-recv", DataType::Int64, false),
                Field::new("usecs-to-first-byte-send", DataType::Int64, false),
                Field::new("usecs-to-last-byte-recv", DataType::Int64, false),
                Field::new("usecs-to-last-byte-send", DataType::Int64, false),
                Field::new("usecs-to-proxy-choice", DataType::Int64, false),
                Field::new("usecs-to-proxy-init", DataType::Int64, false),
                Field::new("usecs-to-proxy-request", DataType::Int64, false),
                Field::new("usecs-to-proxy-response", DataType::Int64, false),
                Field::new("usecs-to-response", DataType::Int64, false),
                Field::new("usecs-to-socket-connect", DataType::Int64, false),
                Field::new("usecs-to-socket-create", DataType::Int64, false),
            ],
            false,
        );

        let transport_info = Field::new_struct(
            "transport_info",
            vec![
                Field::new("error", DataType::Utf8, false),
                Field::new("fd", DataType::Int64, false),
                Field::new("local", DataType::Utf8, false),
                Field::new("proxy", DataType::Utf8, false),
                Field::new("remote", DataType::Utf8, false),
                Field::new("state", DataType::Utf8, false),
            ],
            false,
        );

        let stream_struct = Field::new_struct(
            stream_id,
            vec![
                byte_info,
                elapsed_seconds,
                Field::new("is_complete", DataType::Boolean, false),
                Field::new("is_error", DataType::Boolean, false),
                Field::new("is_success", DataType::Boolean, false),
                Field::new("stream_id", DataType::Utf8, false),
                stream_info,
                time_info,
                transport_info,
                Field::new("unix_ts_start", DataType::Float64, true),
                Field::new("unix_ts_end", DataType::Float64, true),
            ],
            false,
        );

        stream_struct
    }

    /// Generate structs for tgen streams json objects
    fn generate_tgen_streams_structs(streams: &str) -> Vec<Field> {
        let mut vec_streams = Vec::new();
        let lookup_streams: HashMap<String, Value> = serde_json::from_str(&streams).unwrap();
        for key in lookup_streams.keys() {
            vec_streams.push(Self::generate_stream_dictionary(&key));
        }
        vec_streams
    }

    /// Generate dictionary struct for circuit json objects
    fn generate_circuit_dictionary(circuit_id: &str) -> Field {
        let elapsed_seconds = Field::new_list(
            "elapsed_seconds",
            Field::new_list("event", Field::new("item", DataType::Utf8, true), true),
            true,
        );

        let path = Field::new_list(
            "path",
            Field::new_list("router", Field::new("item", DataType::Utf8, true), true),
            true,
        );

        let circuit_struct = Field::new_struct(
            circuit_id,
            vec![
                Field::new("build_quantile", DataType::Float64, true),
                Field::new("build_timeout", DataType::Int32, true),
                Field::new("buildtime_seconds", DataType::Float64, true),
                Field::new("cbt_set", DataType::Boolean, false),
                Field::new("circuit_id", DataType::Int32, false),
                Field::new_list(
                    "current_guards",
                    Field::new_struct(
                        "item",
                        vec![
                            Field::new("country", DataType::Utf8, true),
                            Field::new("fingerprint", DataType::Utf8, true),
                            Field::new("new_ts", DataType::Float64, true),
                            Field::new("nickname", DataType::Utf8, true),
                            Field::new("up_ts", DataType::Float64, true),
                        ],
                        true,
                    ),
                    true,
                ),
                elapsed_seconds,
                Field::new("failure_reason_local", DataType::Utf8, true),
                Field::new("filtered_out", DataType::Boolean, false),
                path,
                Field::new("unix_ts_end", DataType::Float64, true),
                Field::new("unix_ts_start", DataType::Float64, true),
            ],
            false,
        );

        circuit_struct
    }

    /// Generate structs for tgen circuits json objects
    fn generate_tor_circuit_structs(circuits: &str) -> Vec<Field> {
        let mut vec_circuits = Vec::new();
        let lookup_circuits: HashMap<String, Value> = serde_json::from_str(&circuits).unwrap();
        for key in lookup_circuits.keys() {
            vec_circuits.push(Self::generate_circuit_dictionary(&key));
        }
        vec_circuits
    }

    fn read_json(path: &str) -> serde_json::Value {
        let data = fs::read_to_string(path).expect("Unable to read file");
        let res: serde_json::Value = serde_json::from_str(&data).expect("Unable to parse");

        res
    }

    fn get_node_id(res: serde_json::Value, node_id: &mut String) -> &str {
        let node = res["data"].to_string();
        let lookup_node: HashMap<String, Value> = serde_json::from_str(&node).unwrap();
        node_id.push_str(&lookup_node.keys().cloned().collect::<Vec<_>>()[0]);

        &node_id[..]
    }

    pub fn build_schema(&self) -> Arc<arrow_schema::Schema> {
        let node_id = &self.node_id;
        let res = &self.json;
        let streams = res["data"][node_id]["tgen"]["streams"].to_string();
        let circuits = res["data"][node_id]["tor"]["circuits"].to_string();

        let node_struct = Field::new_struct(
            node_id,
            vec![
                Field::new("measurement_ip", DataType::Utf8, false),
                Field::new_struct(
                    "tgen",
                    vec![Field::new_struct(
                        "streams",
                        Self::generate_tgen_streams_structs(&streams),
                        false,
                    )],
                    false,
                ),
                Field::new_struct(
                    "tor",
                    vec![Field::new_struct(
                        "circuits",
                        Self::generate_tor_circuit_structs(&circuits),
                        false,
                    )],
                    false,
                ),
            ],
            false,
        );

        let schema = Arc::new(Schema::new(vec![
            Field::new_struct("data", vec![node_struct], false),
            Field::new_struct(
                "filters",
                vec![Field::new_list(
                    "tor/circuits",
                    Field::new_struct(
                        "item",
                        vec![Field::new("name", DataType::Utf8, false)],
                        false,
                    ),
                    false,
                )],
                false,
            ),
            Field::new("type", DataType::Utf8, false),
            Field::new("version", DataType::Utf8, false),
        ]));

        schema
    }

    pub fn new(path: &str) -> Self {
        let res: serde_json::Value = Self::read_json(&path);
        let mut node_id = String::new();
        let node_id = Self::get_node_id(res.clone(), &mut node_id);
        let json_circuits = res["data"][node_id]["tor"]["circuits"].to_string();
        let lookup_circuits: HashMap<String, Value> = serde_json::from_str(&json_circuits).unwrap();
        let circuits = lookup_circuits.keys().cloned().collect();
        let json_streams = res["data"][node_id]["tgen"]["streams"].to_string();
        let lookup_streams: HashMap<String, Value> = serde_json::from_str(&json_streams).unwrap();
        let streams = lookup_streams.keys().cloned().collect();

        Self {
            node_id: node_id.to_string(),
            path: path.to_string(),
            streams: streams,
            circuits: circuits,
            json: res,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    static PATH: &str = "tests/data/2021-06-01.op-hk6a.onionperf.analysis.json";

    #[test]
    fn test_reading_json_and_parsing_node_id() {
        let res: serde_json::Value = Parse::read_json(&PATH);
        let mut node_id = String::new();
        let node_id = Parse::get_node_id(res.clone(), &mut node_id);
        assert_eq!(node_id, "op-hk6a");
    }

    #[test]
    fn test_reading_json_and_circuits() {
        let res: serde_json::Value = Parse::read_json(&PATH);
        let streams = res["data"]["op-hk6a"]["tgen"]["streams"].to_string();

        assert!(streams.contains("64079:6:localhost:127.0.0.1:35132:ec2-18-162-44-244.ap-east-1.compute.amazonaws.com:18.162.44.244:443"));
    }
}
